import React from "react";

interface Post {
  id: number;
  title: string;
  content: string;
}

interface UserCardProps {
  fname: string;
  lname?: string;
  email: string;
  age: number;
  posts?: [Post] | undefined;
}

const UserCard: React.FC<UserCardProps> = ({
  fname,
  lname,
  email,
  age,
  posts,
}) => {
  const renderPosts = posts?.map((post) => {
    return (
      <div key={post.id}>
        <div>
          <h3>{post.title}</h3>
        </div>
        <div>
          <span>{post.content}</span>
        </div>
      </div>
    );
  });

  return (
    <div>
      <div>
        <h3>UserName:{fname}</h3>
        <h3>UserLastName:{lname}</h3>
      </div>
      <div>
        <span>User Email:{email}</span>
      </div>
      <div>User Age:{age}</div>
      <div>{renderPosts}</div>
    </div>
  );
};

export default UserCard;
