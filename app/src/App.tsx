import React, { useState, useEffect } from "react";
import "./App.css";
import { useMutation, useQuery } from "@apollo/client";
import { GET_ALL_USERS } from "./query/user";
import UserCard from "./components/user-card";

interface Post {
  id: number;
  title: string;
  content: string;
}

interface User {
  id: string;
  fname: string;
  lname: string;
  email: string;
  age: number;
  posts?: [Post] | undefined;
}

function App() {
  const { data, loading, error, refetch } = useQuery(GET_ALL_USERS);
  const [user, setUser] = useState<User[]>([]);

  useEffect(() => {
    if (!loading) {
      setUser(data.getAllUsers);
    }
  }, [data, loading]);

  const renderUserCard = user.map((user) => {
    return (
      <UserCard
        key={user.id}
        fname={user.fname}
        lname={user.lname}
        email={user.email}
        age={user.age}
        posts={user.posts}
      />
    );
  });

  if (loading) {
    return (
      <div>
        <h1>Loading...</h1>
      </div>
    );
  }
  if (error) {
    return (
      <div>
        <h1>Error... :/</h1>
      </div>
    );
  }

  console.log(user);

  return (
    <div className='App'>
      <div>
        <form>
          <input type='text' />
          <input type='text' />
        </form>
        <div>
          <button>Create</button>
          <button>Get</button>
        </div>
        <div>{renderUserCard}</div>
      </div>
    </div>
  );
}

export default App;
