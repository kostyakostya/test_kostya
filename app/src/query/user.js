import { gql } from "@apollo/client";

export const GET_ALL_USERS = gql`
  query {
    getAllUsers {
      id
      fname
      lname
      email
      age
      posts {
        title
        content
      }
    }
  }
`;
